package cabinet;

import java.sql.*;
import javax.swing.*;


public class Database {
	
    private String profile="";
    Connection con = null;
    static PreparedStatement pst;
    ResultSet rs;
    
    public String getProfile() {
        return profile;
    }
    

    public Database(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cabinet?autoReconnect=true&useSSL=false","root","");
            pst=con.prepareStatement("select * from users where login=? and password=?");

        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public Boolean checkLogin(String uname,String pwd)
    {
        try {
                        
            pst.setString(1, uname); //this replaces the 1st  "?" in the query for username
            pst.setString(2, pwd);    //this replaces the 2st  "?" in the query for password
            //executes the prepared statement
            rs=pst.executeQuery();
            if(rs.next())
            {
                profile = rs.getString(4);
                return true;
            }
            else
            {
                return false;
            }
        } catch (Exception e) {
           
            System.out.println("error while validating"+e);
            return false;
        }
    }
}
